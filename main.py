import json

from flask import Flask,render_template,request, url_for
from keras import Model
from keras.layers import Dense, GlobalAveragePooling2D
from werkzeug.utils import redirect
import pickle
from tensorflow import keras
from keras.models import load_model
from tensorflow.keras.utils import load_img
from tensorflow.keras.utils import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras.applications.vgg16 import VGG16


app = Flask(__name__)
#model = keras.models.load_model('C:\Users\Merna\PycharmProjects\New_Project')
model = load_model('VGG16_model.h5')

@app.route('/')
def hello_world():
   return render_template('index.html')



@app.route('/', methods=['POST'])
def predict():
    imagefile = request.files['image']
    image_path = "./" + imagefile.filename
    imagefile.save(image_path)
    #model = load_model(weights='imagenet', include_top=True)

    image = load_img(image_path, target_size=(224, 224))
    image = img_to_array(image)
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    image = preprocess_input(image)
    yhat = model.predict(image)
   #  label = decode_predictions(yhat)
   #  label = label[0][0]
    preds = yhat.tolist()[0]
    print(preds)

    max_value = max(preds) # returns 10
    max_value_index = preds.index(max_value) # retuns 9

    classification = 'Label : %s (%.2f%%)' % (max_value_index, max_value*100)

    return render_template('index.html', prediction=classification)

"""
create a function that does xor to 2 numbers 
"""      
def xor(a, b):
    return (a and not b) or (not a and b)

    
if __name__ == '__main__':
   app.run("127.0.0.1", 5000, True, "Hello")
